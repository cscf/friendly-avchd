# friendly-avchd

End-user friendly conversion of AVCHD video to web .mp4 - meant to be used with a cron job to monitor a user upload directory, and when video files are uploaded, convert them to web-friendly .mp4 and move to an output directory

## Getting Started

git clone, adjust the user variables at the top of avchd_to_mp4, and point a cron job at the avchd_to_mp4 script. 

To use: 

1) drop your AVCHD directory in the `$WORKING_DIR`
2) rename the upload (typically named `'PRIVATE'`) to a destination filename, which must start with a lowercase letter.
   * The upload must contain at least one 'MTS' file. 
3) It will process and email you when it's done.
   * takes approximately 1 minute to process 1 minute of HD video (on our server). You can see progress in `$WORKING_DIR/.logfile` 

### Prerequisites

`ffmpeg` package - tested with ubuntu package version `7:3.4.6-0`

### Installing

git clone

```
git clone ist-git@git.uwaterloo.ca:cscf/friendly-avchd.git
```

Edit all the user variables in `bin/avchd_to_mp4` 

```
WORKING_DIR='/home/user/raw-video'
VIDEO_DIR='/home/user/public_html/video'
VIDEO_EXT="_video.mp4"
VIDEO_URL="https://cs.uwaterloo.ca/~user/video/"
EMAIL_SUBJ="project:"
EMAIL_RECIP="user@uwaterloo.ca,admin@cs.uwaterloo.ca"
```
## Running the script

1) ensure `$WORKING_DIR` contains a directory with AVCHD data, consisting of a main directory whose name is all uppercase (typically `"PRIVATE"`) containing arbitrary subdirectories, one which will contain an arbitrary number of `.MTS` files.

```
user@hostname:~/raw-video$ find PRIVATE
PRIVATE
PRIVATE/SONY
PRIVATE/SONY/SONYCARD.IND
PRIVATE/AVCHD
PRIVATE/AVCHD/BDMV
PRIVATE/AVCHD/BDMV/INDEX.BDM
PRIVATE/AVCHD/BDMV/STREAM
PRIVATE/AVCHD/BDMV/STREAM/00002.MTS
PRIVATE/AVCHD/BDMV/STREAM/00001.MTS
PRIVATE/AVCHD/BDMV/STREAM/00003.MTS
PRIVATE/AVCHD/BDMV/CLIPINF
PRIVATE/AVCHD/BDMV/CLIPINF/00002.CPI
PRIVATE/AVCHD/BDMV/CLIPINF/00000.CPI
PRIVATE/AVCHD/BDMV/CLIPINF/00001.CPI
PRIVATE/AVCHD/BDMV/CLIPINF/00003.CPI
PRIVATE/AVCHD/BDMV/MOVIEOBJ.BDM
PRIVATE/AVCHD/BDMV/PLAYLIST
PRIVATE/AVCHD/BDMV/PLAYLIST/00000.MPL
PRIVATE/M4ROOT
PRIVATE/M4ROOT/THMBNL
PRIVATE/M4ROOT/SUB
PRIVATE/M4ROOT/GENERAL
PRIVATE/M4ROOT/CLIP
PRIVATE/M4ROOT/STATUS.BIN
PRIVATE/M4ROOT/MEDIAPRO.XML
PRIVATE/.DS_Store
```

2) run `./bin/avchd_to_mp4`
   * it will run and return nothing, with no errors.
3) rename the main directory (again, typically `"PRIVATE"`) to anything starting with lowercase
4) run `./bin/avchd_to_mp4`
   * It will process and email you when it's done.
   * takes approximately 1 minute to process 1 minute of HD video (on our server). You can see progress in `$WORKING_DIR/.logfile

## Sample Cron job
We use this, to check every minute:
```
* * * * * /home/user/bin/avchd_to_mp4 
```

## Contributing

Please email drallen@uwaterloo.ca.

## Authors

* **Daniel Allen** - drallen@uwaterloo.ca

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
